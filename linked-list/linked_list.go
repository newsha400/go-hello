package linkedlist

import "fmt"

//ErrEmptyList is thrown when the list is empty
var ErrEmptyList = fmt.Errorf("The list is empty")

//List holds the head and tail nodes of a doubly linked list
type List struct {
	head *Node
	tail *Node
}

//Node holds the value of each node, the pointer to next and previous node, and the list.
type Node struct {
	Val      interface{}
	next     *Node
	previous *Node
	list     *List
}

//Next returns the next node
func (node *Node) Next() *Node {
	if node == nil {
		return nil
	}
	return node.next
}

//Prev returns the previous node
func (node *Node) Prev() *Node {
	if node == nil {
		return nil
	}
	return node.previous

}

//First returns the first node of the list
func (node *Node) First() *Node {
	return node.list.head
}

//Last returns the last node of the list
func (node *Node) Last() *Node {
	return node.list.tail
}

//NewList returns a new list
func NewList(args ...interface{}) *List {
	var list List
	for _, value := range args {
		list.PushBack(value)
	}
	return &list
}

//First returns the first node of the list
func (list *List) First() *Node {
	return list.head
}

//Last returns the last node of the list
func (list *List) Last() *Node {
	return list.tail
}

//PushBack pushes a value from the tail
func (list *List) PushBack(value interface{}) {
	node := Node{
		Val:  value,
		list: list,
	}
	if list.tail == nil {
		list.head = &node
	} else {
		node.previous = list.tail
		list.tail.next = &node
	}
	list.tail = &node
}

//PopBack deletes a value from the tail, and retruns the deleted value
//errors if the list is empty
func (list *List) PopBack() (interface{}, error) {
	var value interface{}
	var err error
	if list == nil {
		return nil, ErrEmptyList
	}
	if list.head == nil || list.tail == nil {
		err = ErrEmptyList
		return nil, err
	}
	value = list.tail.Val
	if list.tail.previous != nil {
		list.tail.previous.next = nil
	}
	list.tail = list.tail.previous
	if list.tail == nil {
		list.head = nil
	}

	return value, err
}

//PushFront pushes a value from the head
func (list *List) PushFront(value interface{}) {
	node := Node{
		Val:  value,
		list: list,
	}
	if list.head == nil {
		list.head = &node
		list.tail = &node
	} else {
		node.next = list.head
		list.head.previous = &node
		list.head = &node
	}
}

//PopFront deletes a value from the head and returns the deleted value
//errors if the list is empty
func (list *List) PopFront() (interface{}, error) {
	var value interface{}
	var err error
	err = nil

	if list.head == nil {
		err = ErrEmptyList
	} else {
		if list.head == nil {
			return nil, fmt.Errorf("head is nil")
		}
		value = list.head.Val
		list.head = list.head.next
		if list.head != nil {
			list.head.previous = nil
		}
	}
	return value, err
}

//Reverse changes the order of the linked list
func (list *List) Reverse() {
	node := list.head
	newList := NewList()
	for {
		if node == nil { //if the list is empty
			break
		} else {
			//fmt.Printf("this is the first node pushed %v\n", node)
			newList.PushFront(node.Val)
			if node.next == nil {
				break
			}
			node = node.next

		}
	}
	list.head = newList.head
	list.tail = newList.tail
}
