package acronym

import (
	"fmt"
	"regexp"
	"strings"
)

// Abbreviate should have a comment documenting it.
func Abbreviate(word string) string {
	var acronym string
	wordByte := []byte(word)
	acronym = string(wordByte[0])
	reg := regexp.MustCompile("( |-)([^A-Za-z]?)([A-Za-z])")
	submatch := reg.FindAllStringSubmatch(word, -1)
	fmt.Println(submatch)
	for i := 0; i < len(submatch); i++ {
		acronym = acronym + submatch[i][3]
	}
	acronym = strings.ToUpper(acronym)
	fmt.Println(acronym)
	return acronym
}
