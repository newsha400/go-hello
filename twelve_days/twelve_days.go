package twelve

import "fmt"

var intro = "On the %v day of Christmas my true love gave to me: "
var verses = make(map[int]string)
var days = make(map[int]string)

func makeVerses() {
	verses[1] = "a Partridge in a Pear Tree"
	verses[2] = "two Turtle Doves"
	verses[3] = "three French Hens"
	verses[4] = "four Calling Birds"
	verses[5] = "five Gold Rings"
	verses[6] = "six Geese-a-Laying"
	verses[7] = "seven Swans-a-Swimming"
	verses[8] = "eight Maids-a-Milking"
	verses[9] = "nine Ladies Dancing"
	verses[10] = "ten Lords-a-Leaping"
	verses[11] = "eleven Pipers Piping"
	verses[12] = "twelve Drummers Drumming"

	days[1] = "first"
	days[2] = "second"
	days[3] = "third"
	days[4] = "fourth"
	days[5] = "fifth"
	days[6] = "sixth"
	days[7] = "seventh"
	days[8] = "eighth"
	days[9] = "ninth"
	days[10] = "tenth"
	days[11] = "eleventh"
	days[12] = "twelfth"
}

//Verse receives the verse line and returns the verse associated to that line
func Verse(verseLine int) string {
	var verse string
	makeVerses()
	var introFormatted string
	introFormatted = fmt.Sprintf(intro, days[verseLine])
	verse = introFormatted
	if verseLine == 1 {
		verse = verse + verses[1] + "."
	} else {
		for i := verseLine; i > 0; i-- {
			if i == 1 {
				verse = verse + "and " + verses[i] + "."
			} else {
				verse = verse + verses[i] + ", "

			}
		}
	}
	//fmt.Println(verse)
	return verse
}

//Song returns the whole song in a single string with newline characters in between the verses
func Song() string {
	var song string
	for i := 1; i <= 12; i++ {
		if i == 12 {
			song = song + Verse(i)
		} else {
			song = song + Verse(i) + "\n"
		}
	}
	//fmt.Println(song)
	return song
}
