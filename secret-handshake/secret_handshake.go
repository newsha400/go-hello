package secret

import "fmt"

//Handshake receives an integer as a secret code and returns the secret handshake according to secret rules
func Handshake(code uint) []string {
	var handshake []string

	var codes = make(map[uint]string)
	codes[1] = "wink"
	codes[1<<1] = "double blink"
	codes[1<<2] = "close your eyes"
	codes[1<<3] = "jump"

	var bit uint = 1
	var i uint
	for i = 0; i < 5; i++ {
		if (code & (bit << i)) == bit<<i {
			if i == 4 {
				//reverse handshake
				var handshakeReverse []string
				for j := len(handshake); j > 0; j-- {
					handshakeReverse = append(handshakeReverse, handshake[j-1])
				}
				handshake = handshakeReverse
			} else {
				handshake = append(handshake, codes[1<<i])
			}
		}
	}
	fmt.Println(handshake)
	return handshake
}
