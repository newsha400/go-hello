package main

import (
	"fmt"
)

func main() {

	var myVariable = myType{
		val: 3,
	}

	myVariable.changeM()
	fmt.Println(myVariable.val)

}

type myType struct {
	val int
}

func (m *myType) changeM() {
	m = &myType{
		val: 2,
	}
	//m.val = 2
}
